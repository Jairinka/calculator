import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Calc1 {
    public static void calc1() {

        double a = 0, b = 0;
        String code = null;

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        while (1 == 1) {
            boolean pr = false;
            while (pr == false) {
                try {
                    System.out.println("Введите операцию");
                    code = reader.readLine();
                    if ((code.equals("+")) || (code.equals("-")) || (code.equals("*")) || (code.equals("/"))
                            || (code.equals("%")) || (code.equals("^")) || (code.equals("root")) || (code.equals("rand")))
                        pr = true;
                    else if ((code.equals("exit")))
                        System.exit(0);
                    else if ((code.equals("help"))) {
                        System.out.println("+ Сложение A и B.");
                        System.out.println("- Вычитание B из A.");
                        System.out.println("* Умножение A на B.");
                        System.out.println("/ Деление A на B.");
                        System.out.println("% Деление по модулю A на B.");
                        System.out.println("^ Возведение A в степень B.");
                        System.out.println("root Вычисление из А корня степени B.");
                        System.out.println("rand Вывод случайного числа от A до B.");
                        System.out.println("help Вывод списка операций");
                        System.out.println("exit Выход из программы.");
                    } else
                        throw new IOException();
                } catch (IOException e) {
                    System.out.println("Ошибка: неверный код операции. Наберите help");
                }
            }

            boolean pra = false;
            while (pra == false) {
                try {
                    System.out.println("Введите первое число");
                    a = Double.parseDouble(reader.readLine());
                    pra = true;
                } catch (NumberFormatException e) {
                    System.out.println("Введеное значение не является числом");
                } catch (IOException e) {
                    System.out.println("Введеное значение не является числом");
                }
            }

            boolean prb = false;
            while (prb == false) {
                try {
                    System.out.println("Введите второе число");
                    b = Double.parseDouble(reader.readLine());
                    prb = true;
                } catch (NumberFormatException e) {
                    System.out.println("Введеное значение не является числом");
                } catch (IOException e) {
                }
            }

            switch (code) {
                case "+":
                    System.out.println(a + b);
                    break;
                case "-":
                    System.out.println(a - b);
                    break;
                case "*":
                    System.out.println(a * b);
                    break;
                case "/":
                    System.out.println(a / b);
                    break;
                case "%":
                    System.out.println(a % b);
                    break;
                case "^":
                    System.out.println(Math.pow(a, b));
                    break;
                case "root":
                    System.out.println(Math.pow(a, (1 / b)));
                    break;
                case "rang":
                    System.out.println((int) ((Math.random() * (b - a)) + a));
                    break;
            }

        }
    }
}